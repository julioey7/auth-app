const securityService = require('../services/security');
const Token = require('../models/token');
const User = require('../models/user');

function validateAuth(request, response, next) {
    if(!request.headers.authorization) {
        return response.status(401).send('com.app.request.error.unauthorized');
    }

    let token = securityService.getCleanToken(request.headers.authorization);
    Token.findOne({token: token}, function(error, instance) {
        if(error || !instance) {
            response.status(401).send('com.app.request.error.unauthorized');
            return;
        }

        if(instance.type === "pre_auth_token" && !request.url.match(new RegExp('/validate/auth', 'i'))) {
            response.status(403).send('com.app.request.error.access.forbidden');
            return;
        }

        User.findOne({email: instance.username}, function(error, user) {
           if(error || !user) {
               response.status(401).send('com.app.request.error.unauthorized');
               return;
           }

           request.locals = {user, token: instance};
           next();
        });
    });
}

module.exports = {
    validateAuth: validateAuth
};