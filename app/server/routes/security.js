const express = require('express');
const router = express.Router();

const securityController = require('../controllers/security');
const authMiddleware = require('../middlewares/authentication');

router.post('/register', securityController.register);
router.post('/login', securityController.login);
router.get('/activate/:token', securityController.activate);
router.get('/password/forgot/:email', securityController.forgotPassword);
router.post('/password/reset/:token', securityController.resetPassword);
router.post('/validate/auth', authMiddleware.validateAuth, securityController.validateAuth);
router.get('/validate/token/:token', securityController.validateToken);
router.post('/logout', authMiddleware.validateAuth, securityController.logout);

module.exports = router;