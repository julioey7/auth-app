const express = require('express');
const userController = require('../controllers/user');
const authMiddleware = require('../middlewares/authentication');
const app = express();

// User routes
app.get('/profile', authMiddleware.validateAuth, userController.profile);




module.exports = app;