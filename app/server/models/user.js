
// USER MODEL

const mongoose = require('mongoose');
const bCrypt = require('bcryptjs');
const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;


let user = new Schema({
    name: {
        type: String,
        required: [true, 'com.app.user.error.name.required'],
    },
    email: {
        type: String,
        required: [true, 'com.app.user.error.email.required'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'com.app.user.error.password.required']
    },
    image: {
        type: String,
        required: false
    },
    role: {
        type: String,
        default: 'ROLE_USER'
    },
    status: {
        type: Boolean,
        required: true,
        default: false
    },
    two_step_auth: {
        type: Boolean,
        required: true,
        default: true
    },
    google: {
        type: Boolean,
        required: true,
        default: false
    }
});

user.plugin(uniqueValidator, {message: 'com.app.user.error.{PATH}.duplicated'});
user.methods.toJSON = function () {
    let user = this;
    let userObject = user.toObject();
    delete userObject.password;

    return userObject;
};

user.pre('save', function (next) {
    if (this.isModified('password') || this.isNew) {
        this.password = bCrypt.hashSync(this.password, 10);
        next();
    } else {
        return next();
    }
});


user.methods.comparePassword = function (password, callback) {
    bCrypt.compare(password, this.password, function (error, isMatch) {
        if (error) {
            return callback(error);
        }
        callback(null, isMatch);
    });
};

module.exports = mongoose.model('User', user);