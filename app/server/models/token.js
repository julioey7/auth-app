const mongoose  = require('mongoose');
const randToken = require('rand-token').generator();
const Schema = mongoose.Schema;

let token = Schema({
    token: {
        type: String,
        required: true,
        default: function () {
            return randToken.generate(32);
        }
    },
    type: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    latitude: {
        type: String,
        required: false
    },
    longitude: {
        type: String,
        required: false
    },
    browser: {
        type: String,
        required: true,
        default: 'unknown'
    },
    os: {
        type: String,
        required: true,
        default: 'unknown'
    },
    device: {
        type: String,
        required: true,
        default: 'unknown'
    },
    socket: {
        type: String,
        required: false,
        default: null
    }
});

module.exports = mongoose.model('Token', token);