const { io } = require('../../app');
const tokenController = require('../controllers/token');


io.on('connection', (client) => {

    client.on('startConnection', (data) => {
        console.log("connecting >>", client.id);
        tokenController.setOnline(data, client.id, (error, result) => {
            if(error) {
                console.error('Error setting user online >>', error);
                return client.emit('updateOnlineListError', error);
            }

            tokenController.onlineUsers((error, list) => {
                if(error) {
                    return client.emit('gettingOnlineListError', error);
                }
                client.emit('updateOnlineList', list);
                client.broadcast.emit('updateOnlineList', list);
            });
        });
    });

    client.on('disconnect', () => {
        console.log("disconnecting >>", client.id);
        tokenController.setOffline(client.id, (error, result) => {
            if(error) {
                console.error('Error setting user online >>', error);
                return client.emit('updateOnlineListError', error);
            }

            tokenController.onlineUsers((error, list) => {
                if(error) {
                    return client.emit('gettingOnlineListError', error);
                }
                client.emit('updateOnlineList', list);
                client.broadcast.emit('updateOnlineList', list);
            });
        });
    });

    client.on('sendMessage', (message) => {
        client.broadcast.emit('sendMessage', message);
    });
});