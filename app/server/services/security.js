function getCleanToken(token) {
    if(!token) return null;
    return token.replace('bearer ', '') || null;
}

module.exports = {
    getCleanToken: getCleanToken
};