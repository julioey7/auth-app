const nodemailer = require('nodemailer');
const handlebars = require('handlebars');
const fs = require('fs');
const path = require('path');

function readHTML(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, (error, html) => {
        if(error) {
            callback(error);
            return;
        }

        callback(null, html);
    });
}

function sendEmail(params, callback) {
    if(!params.type || !params.user) {
        callback('Missing params in sendMail service');
        return;
    }

    let templateUrl;
    let subject;

    switch (params.type) {
        case 'sign_up':
            templateUrl = path.join(__dirname, '../', 'templates/' + params.locale, '/_register.html');
            subject = params.locale === 'en_US'? "New account | auth app" : "Nueva cuenta | auth app";
            break;
        case 'restore_password':
            templateUrl = path.join(__dirname, '../', 'templates/' + params.locale, '/_forgotPassword.html');
            subject = params.locale === 'en_US'? "Restore password | auth app" : "Restaurar contraseña | auth app";
            break;
        case 'validate_auth':
            templateUrl = path.join(__dirname, '../', 'templates/' + params.locale, '/_auth.html');
            subject = params.locale === 'en_US'? "Validate session | auth app" : "Validar sesión | auth app";
            break;
        default:
            templateUrl = null;
            subject = null;
            break;
    }

    if(!templateUrl || !subject) {
        callback('Invalid type of mail in Mail service');
        return;
    }

    readHTML(templateUrl, (error, html) => {
        if(error) {
            callback(error);
            return;
        }

        let mailing = {
            service: 'gmail',
                auth: {
                user: 'appmailingizy@gmail.com',
                    pass: '#izy@1234.'
            }
        };

        let transporter = nodemailer.createTransport(mailing);

        let template = handlebars.compile(html);
        let replacements = {
            name: params.user.name,
            url: params.user.url,
            code: params.user.code
        };

        let htmlToSend = template(replacements);

        let options = {
            from: mailing.auth.user,
            to: params.user.email,
            subject: subject,
            html: htmlToSend
        };

        transporter.sendMail(options, (error, info) => {
            if(error) {
                callback(error);
                return;
            }

            callback(null, info);
        });
    });
}

module.exports = {
    sendEmail
};