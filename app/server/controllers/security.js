const User = require('../models/user');
const Token = require('../models/token');
const mailService = require('../services/mailing');
const AuthenticationCode = require('../models/authenticationCode');

function register(request, response) {
    let json = request.body;
    if(!json.name || !json.email || !json.password) {
        let field = !json.name? 'name': !json.email? 'email': 'password';
        response.status(409).send(`com.app.request.error.${field}.required`);
        return
    }

    json.active = false;
    json.role = 'ROLE_USER';
    json.two_step_auth = true;

    new User(json).save((error, user) => {
        if(error) {
            // console.error('Error saving user: ', error);
            response.status(409).send({error});
            return;
        }

        new Token({
            type: "registration_token",
            username: user.email,
            device: json.device.device,
            os: json.device.os,
            browser: json.device.browser,
            latitude: json.location? json.location.latitude || null : null,
            longitude: json.location? json.location.longitude || null: null
        }).save((error, token) => {

            let params = {
                user: {email: user.email, name: user.name, url: `${request.get('referer')}#!/security/activate/${token.token}`},
                type: 'sign_up',
                locale: request.headers['accept-language'] || 'en_US'
            };

            mailService.sendEmail(params, (error, result) => {
                if(error) {
                    response.status(500).send({error});
                    return;
                }

                response.status(201).send('com.app.request.success.user.created');
            });
        });
    });
}

function login(request, response) {
    let json = request.body;
    if(!json.username || !json.password) {
        let field = !json.username? 'username' : 'password';
        response.status(409).send('com.app.request.error.'+field+'.required');
        return
    }

    User.findOne({email: json.username.toLowerCase()}, (error, user) => {
        if(error || !user) {
            response.status(401).send('com.app.login.error.unauthorized');
            return
        }

        if(!user.status) {
            response.status(403).send('com.app.login.error.inactive.account');
            return;
        }

        user.comparePassword(json.password, (error, match) => {
            if(error) {
                console.error('Error comparing passwords:', error);
                response.status(500).send({error});
                return;
            }

            if(!match) {
                response.status(401).send('com.app.login.error.unauthorized');
                return;
            }

            let tokenType = user.two_step_auth? 'pre_auth_token' : 'access_token';

            new Token({
                type: tokenType,
                username: user.email,
                device: json.device.device,
                os: json.device.os,
                browser: json.device.browser,
                latitude: json.location? json.location.latitude || null : null,
                longitude: json.location? json.location.longitude || null: null
            }).save((error, token) => {
                if(error || !token) {
                    console.error('Error generating token:', error);
                    response.status(500).send({error});
                    return;
                }

                if(tokenType === 'pre_auth_token') {
                    new AuthenticationCode({username: user.email}).save((error, authCode) => {
                        if(error || !authCode) {
                            console.error('Error creating auth code:', error);
                            response.status(500).send({error});
                            return;
                        }

                        let params = {
                            user: {email: user.email, name: user.name, code: authCode.code},
                            type: 'validate_auth',
                            locale: request.headers['accept-language'] || 'en_US'
                        };

                        mailService.sendEmail(params, (error, result) => {
                            if(error) {
                                response.status(500).send({error});
                                return;
                            }

                            response.status(200).send({
                                username: user.email,
                                authorization: "bearer " + token.token,
                                role: "PRE_AUTH"
                            });
                        });
                    });
                    return;
                }

                response.status(200).send({
                    username: user.email,
                    authorization: "bearer " + token.token,
                    role: user.role
                });
            });
        });
    });
}

function activate(request, response) {
    let activationToken = request.params.token;
    if(!activationToken) {
        response.status(409).send('com.app.request.error.token.required');
        return;
    }

    Token.findOneAndRemove({ token: activationToken }, (error, token) => {
        if(!token) {
            response.status(404).send('com.app.response.error.token.not.found');
            return;
        }

        if(token.type !== 'registration_token') {
            response.status(409).send('com.app.response.error.token.type.invalid');
            return;
        }

        User.findOneAndUpdate({ email: token.username }, {status: true}, (error, user) => {
            if(error) {
                console.error(error);
                response.status(500).send({error});
                return;
            }

            if(!user) {
                response.status(404).send('com.app.response.error.user.not.found');
                return;
            }

            response.status(200).send('com.app.response.success.account.activated');
        });
    });
}

function forgotPassword(request, response) {
    let email = request.params.email;
    if(!email) {
        response.status(409).send('com.app.request.error.email.required');
        return;
    }

    User.findOne({email: email}, (error, user) => {
        if(error) {
           console.error('Error getting user:', error);
           response.status(500).send({error});
           return;
        }
        if(!user) {
           response.status(404).send('com.app.response.error.user.not.found');
           return;
        }
        new Token({type: 'recover_password', username: email}).save((error, instance) => {
            if(error || !instance) {
                console.error('Error creating token:', error);
                response.status(500).send({error});
                return null;
            }

            let params = {
                user: {email: user.email, name: user.name, url: `${request.get('referer')}#!/security/password/reset/${instance.token}`},
                type: 'restore_password',
                locale: request.headers['accept-language'] || 'en_US'
            };

            mailService.sendEmail(params, (error, result) => {
                if(error) {
                    response.status(500).send({error});
                    return;
                }

                response.status(201).send('com.app.request.success.recovery.email.sent');
            });
        });
    });
}

function resetPassword(request, response) {
    let json = request.body;
    let token = request.params.token;
    if(!token || !json.password || !json.password2) {
        let field = !json.password? 'password' : !json.password2? 'password2' : 'token';
        response.status(409).send('com.app.request.error.' + field + '.required');
        return;
    }

    if(json.password !== json.password2) {
        response.status(409).send('com.app.request.error.password.mismatch');
        return;
    }

    Token.findOneAndRemove({ token: token }, (error, instance) => {
        if(error) {
            console.error('Error getting token:', error);
            response.status(500).send({error});
            return;
        }

        if(!instance) {
            response.status(404).send('com.app.response.error.token.not.found');
            return;
        }

        User.findOneAndUpdate({email: instance.email}, {password: json.password}, (error, user) => {
            if(error) {
                console.error('Error updating password:', error);
                response.status(500).send({error});
                return;
            }

            if(!instance) {
                response.status(404).send('com.app.response.error.user.not.found');
                return;
            }

            response.status(200).send('com.app.response.success.password.updated');
        });
    });
}

function validateToken(request, response) {
    let token = request.params.token;
    if(!token) {
        response.status(409).send('com.app.request.error.email.required');
        return;
    }

    Token.findOne({ token: token }, (error, instance) => {
        if(error) {
            console.error('Error getting token:', error);
            response.status(500).send({error});
            return;
        }

        if(!instance) {
            response.status(404).send('com.app.response.error.token.not.found');
            return;
        }

        console.log(instance);

        if(instance.type !== 'recover_password') {
            response.status(409).send('com.app.response.error.token.type.invalid');
            return;
        }

        response.status(200).send('com.app.response.success.token.valid')
    });
}

function validateAuth(request, response) {
    if(request.locals.token.type !== 'pre_auth_token') {
        response.status(403).send('com.app.request.error.access.forbidden');
        return;
    }

    if(!request.body.code) {
        response.status(409).send('com.app.request.error.code.required');
        return;
    }

    AuthenticationCode.findOneAndRemove({ code: parseInt(request.body.code), username: request.locals.token.username }, (error, instance) => {
        if(error) {
            console.error('Error getting authentication code:', error);
            response.status(500).send({error});
            return;
        }

        if(!instance) {
            response.status(404).send('com.app.response.error.authentication.invalid');
            return;
        }

        Token.findByIdAndUpdate(request.locals.token._id, { type: 'access_token'}, (error, token) => {
            if(error) {
                console.error('Error updating token', error);
                response.status(500).send({error});
                return;
            }

            if(!token) {
                response.status(404).send('com.app.response.error.token.not.found');
                return;
            }

            response.status(200).send({username: token.username, authorization: "bearer " + token.token, role: request.locals.user.role});
        });
    });
}

function logout(request, response) {
    console.info('Logout service:', request.locals);
    Token.findByIdAndRemove(request.locals.token._id, (error, instance) => {
        if(error) {
            console.error('Error removing token:', error);
            response.status(500).send({error});
            return;
        }

        if(!instance) {
            response.status(404).send('com.app.response.error.token.not.found');
            return;
        }

        response.status(200).send('com.app.response.success.user.logged.out');
    });
}

module.exports = {
    register,
    login,
    logout,
    activate,
    forgotPassword,
    resetPassword,
    validateAuth,
    validateToken
};