const User = require('../models/user');

// Service to get profile from current user
function profile(request, response) {
    response.status(200).send(request.locals.user)
}

module.exports = {
    profile
};