const _ = require('lodash');
// TOKEN CONTROLLER
const Token = require('../models/token');
const User = require('../models/user');

let onlineUsers = (callback) => {
    Token.find({socket: {$ne: null}}, (error, list) => {

        let emails = [], excludes = {google: 0, two_step_auth: 0, status: 0, role: 0};
        _.each(list, token => emails.push(token.username));
        User.find({'email': {$in: emails}}, excludes, (error, users) => {
            callback(error, users);
        });
    });
};

let setOnline = (token, socket, callback) => {
    Token.findOneAndUpdate(token, {socket}, (error, token) => {
        callback(error, token)
    });
};

let setOffline = (socket, callback) => {
    Token.findOneAndUpdate({socket}, {socket: null}, (error, token) => {
        callback(error, token)
    });
};

module.exports = {
    onlineUsers,
    setOnline,
    setOffline
};