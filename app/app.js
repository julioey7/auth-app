require('./config/config');


const express = require('express');
const mongoose = require('mongoose');
const socketIO = require('socket.io');
const http = require('http');

const app = express();
const bodyParser = require('body-parser');

let server = http.createServer(app);

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


app.use( express.static( __dirname + '/public' ) );

// DB Connection
mongoose.connect(process.env.DBURL, { useNewUrlParser: true }, (error, response) => {
    if(error) throw error;
    console.info(`Connection established with database`);
});

// Config headers
app.use((request, response, next)=> {
    response.header('Access-Control-Allow-Origin', '*');
    response.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Accept, Access-Control-Allow-Request-Methods');
    response.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    response.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// Routes
app.use('/security', require('./server/routes/security'));
app.use('/user', require('./server/routes/user'));

// IO = keep connection with backend
module.exports.io = socketIO(server);
require('./server/sockets/chat');
server.listen(process.env.PORT, () => console.info(`Server started at port ${ process.env.PORT }`));