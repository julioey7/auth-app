

// GLOBAL CONFIGURATIONS


// PORT
process.env.PORT = process.env.PORT || 8080;


// ENVIRONMENT
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// DATABASE
process.env.DBURL = process.env.DBURL || 'mongodb://localhost:27017/auth-app';