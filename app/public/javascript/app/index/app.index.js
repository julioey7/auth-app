

angular.module('app.index', ['ui.router'])
    .config(config);


function config($stateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('index');
    _.each(indexStates(), function (state) {
        $stateProvider.state(state);
    });
}

function indexStates() {
    return {
        'index': {
            name: 'index',
            url: '/',
            templateUrl: '/javascript/app/index/templates/index.html',
            controller: 'indexController',
            controllerAs: 'vm',
            abstract: true,
            resolve: {
                lazyLoad: function($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: [
                                {type: 'js', path: '/javascript/app/core/services/translate.js'},
                                {type: 'js', path: '/javascript/app/index/controllers/index.js'}
                            ]
                        }
                    ])
                }
            }
        },
        'index.home': {
            name: 'index.home',
            url: 'home',
            templateUrl: '/javascript/app/index/templates/home.html'
        },
        'index.404': {
            name: 'index.404',
            url: '404',
            templateUrl: '/javascript/app/index/templates/404.html'
        }
    }
}