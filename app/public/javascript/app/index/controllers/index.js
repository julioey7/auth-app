

angular.module('app.index')
    .controller('indexController', indexController);

function indexController(localStorageService, translateService) {
    let vm = this;
    vm.translate = translateService;
    vm.translate.use('index');
}