

angular.module('app.security', [])
    .config(config);

function config($stateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('security');
    _.each(securityStates(), function(state) {
        $stateProvider.state(state);
    })
}


function securityStates() {
    return {
        'security': {
            name: 'security',
            url: '/security',
            templateUrl: '/javascript/app/security/templates/security.html',
            controller: 'securityController',
            controllerAs: 'vm',
            abstract: true,
            resolve: {
                lazyLoad: function($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: [
                                {type: 'js', path: '/javascript/app/core/services/translate.js'},
                                {type: 'js', path: '/javascript/app/core/services/validate.js'},
                                {type: 'js', path: '/javascript/app/security/controllers/security.js'}
                            ]
                        },
                        {
                            serie: true,
                            name: 'ngMessages',
                            files: [
                                {type: 'js', path: '/javascript/dependencies/angular-messages.min.js'}
                            ]
                        },
                        {
                            serie: true,
                            name: 'toastr',
                            files: [
                                {type: 'js', path: '/javascript/dependencies/angular-toastr.tpls.min.js'},
                                {type: 'js', path: '/javascript/app/core/config/toastr.js'},
                                {type: 'css', path: '/stylesheets/angular-toastr.css'}
                            ]
                        },
                        {
                            serie: true,
                            name: 'ng.deviceDetector',
                            files: [
                                {type: 'js', path: '/javascript/dependencies/re-tree.min.js'},
                                {type: 'js', path: '/javascript/dependencies/ua-device-detector.min.js'},
                                {type: 'js', path: '/javascript/dependencies/ng-device-detector.min.js'}
                            ]
                        },
                        {
                            serie: true,
                            name: 'geolocation',
                            files: [
                                {type: 'js', path: '/javascript/dependencies/angularjs-geolocation.min.js'}
                            ]
                        },
                    ]);
                }
            }
        },
        'security.register': {
            name: 'security.register',
            url: '/register',
            templateUrl: '/javascript/app/security/templates/register.html',
            controller: 'registerController',
            controllerAs: 'vm',
            resolve: {
                lazyLoad: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('/javascript/app/security/controllers/register.js')
                }
            }
        },
        'security.activate': {
            name: 'security.activate',
            url: '/activate/:token',
            templateUrl: '/javascript/app/security/templates/activate.html',
            controller: 'activateController',
            controllerAs: 'vm',
            resolve: {
                lazyLoad: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('/javascript/app/security/controllers/activate.js')
                },
                activation: activate
            }
        },
        'security.login': {
            name: 'security.login',
            url: '/login',
            templateUrl: '/javascript/app/security/templates/login.html',
            controller: 'loginController',
            controllerAs: 'vm',
            resolve: {
                lazyLoad: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('/javascript/app/security/controllers/login.js')
                }
            }
        },
        'security.validate': {
            name: 'security.validate',
            url: '/validate/auth',
            templateUrl: '/javascript/app/security/templates/validate.html',
            controller: 'validateController',
            controllerAs: 'vm',
            resolve: {
                lazyLoad: function($ocLazyLoad) {
                    return $ocLazyLoad.load('/javascript/app/security/controllers/validate.js');
                }
            }
        },
        'security.forgot': {
            name: 'security.forgot',
            url: '/password/forgot',
            templateUrl: '/javascript/app/security/templates/forgot.html',
            controller: 'forgotController',
            controllerAs: 'vm',
            resolve: {
                lazyLoad: function($ocLazyLoad) {
                    return $ocLazyLoad.load('/javascript/app/security/controllers/forgot.js');
                }
            }
        },
        'security.reset': {
            name: 'security.reset',
            url: '/password/reset/:token',
            templateUrl: '/javascript/app/security/templates/reset.html',
            controller: 'resetController',
            controllerAs: 'vm',
            resolve: {
                lazyLoad: function($ocLazyLoad) {
                    return $ocLazyLoad.load('/javascript/app/security/controllers/reset.js');
                },
                validation: validateToken
            }
        }
    }
}

function activate(lazyLoad, appService, $q, $stateParams) {
    return $q.when(
        appService.callService('GET',`security/activate/${$stateParams.token}`, null, false).then((response) => {
            return response;
        }, (error) => {
            return error;
        })
    );
}

function validateToken(lazyLoad, appService, $q, $stateParams) {
    return $q.when(
        appService.callService('GET',`security/validate/token/${$stateParams.token}`, null, false).then((response) => {
            return response;
        }, (error) => {
            return error;
        })
    );
}