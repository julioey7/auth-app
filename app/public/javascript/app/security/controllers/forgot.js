

angular.module('app.security')
    .controller('forgotController', forgotController);

function forgotController(validateService, appService, toastr, $translate, deviceDetector, geolocation) {
    let vm = this;
    vm.validate = validateService;
    vm.submit = submit;
    vm.data = {};

    vm.data.device = {
        os: deviceDetector.os,
        browser: deviceDetector.browser,
        device: deviceDetector.device
    };

    geolocation.getLocation().then((data)=> {
        vm.data.location = {latitude: data.coords.latitude, longitude: data.coords.longitude}
    });

    function submit() {
        vm.submitting = true;
        appService.callService('GET', `/security/password/forgot/${vm.data.email}`, null, false).then((response) => {
            console.log('User registered >>', response);
            vm.success = true;
            vm.submitting = false;
        }, (response) => {
            console.error('Error registering user >>', response.data.error.errors);
            if(response.data.error.errors.email) {
                toastr.error($translate.instant(response.data.error.errors.email.message));
            } else {
                toastr.error($translate.instant('com.app.response.default.error'));
            }
            vm.submitting = false;
        })
    }
}