

angular.module('app.security')
    .controller('validateController', validateController);

function validateController(appService, localStorageService, validateService, toastr, $translate, $state) {
    let vm = this;
    vm.validate = validateService;

    vm.data = {
        email: localStorageService.get('username')
    };

    vm.submit = submit;

    function submit() {
        appService.callService('POST', 'security/validate/auth', vm.data, true).then((response) => {
            console.info('Auth validated >>', response.data);
            $state.go('user.home');
        }, (response) => {
            toastr.error($translate.instant(response.data));
            console.error('Error validating auth >>', response.data);
        });
    }
}