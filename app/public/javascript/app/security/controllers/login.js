

angular.module('app.security')
    .controller('loginController', registerController);

function registerController(validateService, appService, toastr, $translate, localStorageService, $state, geolocation, deviceDetector) {
    let vm = this;
    vm.validate = validateService;
    vm.submit = submit;
    vm.data = {};

    geolocation.getLocation().then((data)=> {
        vm.data.location = {lat: data.coords.latitude, long: data.coords.longitude}
    });

    vm.data.device = {
        os: deviceDetector.os,
        browser: deviceDetector.browser,
        device: deviceDetector.device
    };

    function submit() {
        vm.submitting = true;
        appService.callService('POST', 'security/login', vm.data, false).then(function (response) {
            localStorageService.set('authorization', response.data.authorization);
            localStorageService.set('username', response.data.username);
            $state.go('security.validate');
            vm.submitting = false;
        }, function (response) {
            toastr.error($translate.instant(response.data));
            console.error('Login failed >>', response.data);
            vm.submitting = false;
        })
    }
}