

angular.module('app.security')
    .controller('securityController', securityController);

function securityController(translateService) {
    let vm = this;
    vm.translate = translateService;
    vm.translate.use('security');
}