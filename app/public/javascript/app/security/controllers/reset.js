

angular.module('app.security')
    .controller('resetController', registerController);

function registerController(validation, validateService, appService, toastr, $translate, $stateParams) {
    let vm = this;
    vm.validate = validateService;
    vm.submit = submit;
    vm.data = {};

    vm.error = validation.status !== 200;

    function submit() {
        vm.submitting = true;
        appService.callService('POST', `/security/password/reset/${$stateParams.token}`, vm.data, false).then((response) => {
            console.log('User registered >>', response);
            vm.success = true;
            vm.submitting = false;
        }, (response) => {
            console.error('Error resetting password >>', response.data);
            if(response.data.error.errors.email) {
                toastr.error($translate.instant(response.data.error.errors.email.message));
            } else {
                toastr.error($translate.instant('com.app.response.default.error'));
            }
            vm.submitting = false;
        })
    }
}