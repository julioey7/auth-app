

angular.module('app', [
    'btford.socket-io',
    'ui.router',
    'oc.lazyLoad',
    'pascalprecht.translate',
    'angular-loading-bar',
    'LocalStorageModule',
    'ngSanitize',
    'ngAnimate',
    'angularCSS',
    'app.core',
    'app.index',
    'app.security',
    'app.user'
    ])
    .config(config)
    .run(run);

function config(
    $ocLazyLoadProvider,
    $urlRouterProvider,
    $translateProvider,
    $translatePartialLoaderProvider,
    cfpLoadingBarProvider,
    localStorageServiceProvider
) {
    $urlRouterProvider.when('', '/home');
    $urlRouterProvider.when('/', '/home');
    $urlRouterProvider.otherwise('/404');

    $translateProvider.useLoader('$translatePartialLoader', {
        urlTemplate: '/i18n/{part}/{lang}.json'
    });

    $translateProvider.preferredLanguage('en_US');
    $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

    cfpLoadingBarProvider.includeSpinner = false;

    localStorageServiceProvider.setPrefix('auth-app');

    $ocLazyLoadProvider.config({
        'debug': false,
        'events': true
    });
}

function run($trace, $transitions, $css) {
    $trace.enable('TRANSITION');
    $transitions.onExit({}, (transition) => {
        let from = transition.from().name.split('.')[0], to = transition.to().name.split('.')[0];
        if(from && from !== to) {
            $css.remove(`stylesheets/${from}/styles.css`);
            $css.remove(`stylesheets/${from}/mobile.css`);
        }
    });

    $transitions.onBefore({}, (transition) => {
        let from = transition.from().name.split('.')[0], to = transition.to().name.split('.')[0];
        if(from !== to) {
            $css.add(`stylesheets/${to}/styles.css`);
            $css.add(`stylesheets/${to}/mobile.css`);
        }
    });

    $transitions.onError({}, (transition) => {
        let from = transition.from().name.split('.')[0], to = transition.to().name.split('.')[0];
        if(from && from !== to) {
            $css.remove(`stylesheets/${to}/styles.css`);
            $css.remove(`stylesheets/${to}/mobile.css`);
        }
        if(transition._error.detail.status === 401) {
            transition.router.stateService.transitionTo('security.login');
            return true;
        }
    })
}