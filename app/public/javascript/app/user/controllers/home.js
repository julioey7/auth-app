

angular.module('app.user')
    .controller('homeController', homeController);

function homeController($scope, appSocket, localStorageService) {
    let vm = this;

    // vm.menu = $scope.$parent.vm.menu;

    vm.send = send;
    vm.profile = $scope.$resolve.profile;
    vm.messages = [];
    vm.token = localStorageService.get('authorization').replace('bearer ', '');

    appSocket.on('connect', ()=> {
        vm.connection = true;
        appSocket.emit('startConnection', {token: vm.token})
    });

    appSocket.on('disconnect', ()=> {
        vm.connection = false;
    });

    appSocket.on('sendMessage', (message) => {
        vm.messages.push(message);
        console.log("Chat window >>", angular.element(document.getElementById('chat'))[0].scrollHeight);
        console.log("Chat window >>", angular.element(document.getElementById('chat')));
    });

    appSocket.on('updateOnlineList', (list) => {
        console.log('OnlineUsers >>', list);
        vm.onlineUsers = list;
    });

    function send() {
        let data = {
            user: {id: vm.profile.id, name: vm.profile.name, image: vm.profile.image, email: vm.profile.email},
            message: vm.data.message
        };
        appSocket.emit('sendMessage', data);
        vm.messages.push(data);
        vm.data = {};
    }
}