

angular.module('app.user')
    .controller('userController', userController);

function userController(profile, translateService) {
    let vm = this;
    vm.translate = translateService;
    vm.translate.use('user');

    vm.profile = profile;
    vm.menu = true;
}