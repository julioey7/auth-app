

angular.module('app.user', [])
    .config(config);

function config($stateProvider, $translatePartialLoaderProvider) {
    $translatePartialLoaderProvider.addPart('user');
    _.each(userStates(), function(state) {
        $stateProvider.state(state);
    })
}

function userStates() {
    return {
        'user': {
            name: 'user',
            url: '/user',
            templateUrl: '/javascript/app/user/templates/user.html',
            controller: 'userController',
            controllerAs: 'vm',
            abstract: true,
            resolve: {
                lazyLoad: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            serie: true,
                            files: [
                                {type: 'js', path: '/javascript/app/core/services/app.js'},
                                {type: 'js', path: '/javascript/app/core/services/translate.js'},
                                {type: 'js', path: '/javascript/app/user/controllers/user.js'}
                            ]
                        },
                        {
                            serie: true,
                            name: 'toastr',
                            files: [
                                {type: 'js', path: '/javascript/dependencies/angular-toastr.tpls.min.js'},
                                {type: 'js', path: '/javascript/app/core/config/toastr.js'},
                                {type: 'css', path: '/stylesheets/angular-toastr.css'}
                            ]
                        },
                        {
                            serie: true,
                            name: 'duScroll',
                            files: [
                                {type: 'js', path: '/javascript/dependencies/angular-scroll.min.js'}
                            ]
                        },
                        {
                            serie: true,
                            name: 'ng.deviceDetector',
                            files: [
                                {type: 'js', path: '/javascript/dependencies/re-tree.min.js'},
                                {type: 'js', path: '/javascript/dependencies/ua-device-detector.min.js'},
                                {type: 'js', path: '/javascript/dependencies/ng-device-detector.min.js'}
                            ]
                        },
                        {
                            serie: true,
                            name: 'geolocation',
                            files: [
                                {type: 'js', path: '/javascript/dependencies/angularjs-geolocation.min.js'}
                            ]
                        },
                    ]);
                },
                profile: profile
            }
        },
        'user.home': {
            name: 'user.home',
            url: '/home',
            templateUrl: '/javascript/app/user/templates/home.html',
            controller: 'homeController',
            controllerAs: 'vm',
            resolve: {
                lazyLoad: function ($ocLazyLoad) {
                    return $ocLazyLoad.load('/javascript/app/user/controllers/home.js')
                }
            }
        }
    }
}


function profile(lazyLoad, appService, $q) {
    return $q.when(
        appService.callService('GET',`user/profile/`, null, true).then((response) => {
            console.log('Loading user >>', response);
            return response.data;
        }, (error) => {
            return $q.reject({status: error.status, message: error.data});
        })
    );
}