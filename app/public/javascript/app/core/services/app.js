

angular.module('app.core')
    .service('appService', appService);

function appService($http, localStorageService) {

    return {
        callService: callService
    };

    function callService(type, url, data, auth) {
        return $http({
            method: type,
            url: url,
            headers: {
                'Content-Type' : 'application/json',
                'Authorization': auth? localStorageService.get('authorization') : null,
                'Accept-Language': localStorageService.get('lang') || 'en_US'
            },
            data:  data
        })
    }
}