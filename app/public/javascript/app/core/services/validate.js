

angular.module('app.core')
    .service('validateService', validateService);

function validateService() {
    var vm = this;
    vm.regex = {
        email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,5}))$/,
        password: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d\W]{8,}$/,
        name: /(-?([\w].\s)?([\w']+)\s?)+([A-Z]'([A-Z][a-z]+))?/,
        code: /(\d){5,6}/
    };
    vm.form = form;
    vm.onlyNumbers = onlyNumbers;
    vm.modelOptions = {
        debounce: 500,
        allowInvalid: true
    };

    return vm;

    function form(form) {
        _.each(form.$$controls, function(field){
            field.$setTouched();
        });
        return !form.$valid;
    }

    function onlyNumbers(event) {
        if (event.keyCode < 48 || event.keyCode > 57)
            event.preventDefault();
    }
}