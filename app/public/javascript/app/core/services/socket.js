

angular.module('app.core')
    .factory('appSocket', appSocket);

function appSocket(socketFactory) {
    return socketFactory();
}