

angular.module('app.core')
    .service('translateService', translate);

function translate(localStorageService, $translatePartialLoader, $translate) {

    let vm = this;

    vm.get = get;
    vm.use = use;
    vm.setLang = setLang;
    setLang(localStorageService.get('lang') || 'en_US');

    return vm;

    function get() {
        return {
            lang: localStorageService.get('lang')
        }
    }

    function use(part) {
        $translatePartialLoader.addPart(part);
    }

    function setLang(lang) {
        localStorageService.set('lang', lang);
        $translate.use(lang);
        vm.current = lang;
    }
}